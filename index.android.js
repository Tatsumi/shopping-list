/**
 * Gingerasia Shopping list
 * Copy: Tatsumi Suzuki, Cecilia Fredriksson
 * Date: 2016-07-03
 */
import React, { Component } from 'react';
import {
	AppRegistry,
	Navigator
} from 'react-native';

import ListContainer from "./components/ListContainer.js";
import SingleComponent from "./components/SingleComponent.js";

class ShoppingList extends Component {

	renderScene(route, navigator) {
		if(route.name === 'Shoppinglist') {
			return <ListContainer navigator={navigator} title={route.name} />
		}
		else {
			return <SingleComponent navigator={navigator} title={route.name} />
		}
	}
	
	render() {
		return (
			<Navigator
				initialRoute={{ name: 'Shoppinglist', index: 0 }}
				renderScene={this.renderScene} />
		);
	}
}

AppRegistry.registerComponent('shoppinglist', () => ShoppingList);
