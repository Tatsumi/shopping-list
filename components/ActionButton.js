/**
 * Gingerasia Shopping list
 * Copy: Tatsumi Suzuki, Cecilia Fredriksson
 * Date: 2016-07-05
 */

import React, { Component } from 'react';

import {
	StyleSheet,
	Text,
	View,
	TouchableHighlight
} from 'react-native';

class ActionButton extends React.Component {
	render() {
		return (
			<TouchableHighlight onpress="{this.props.onPress}">
				<Text style={styles.actionColor}>{this.props.title}</Text>
			</TouchableHighlight>
		);
	}
}

const styles = StyleSheet.create({
	action: {
		flex: 1,
		backgroundColor: '#FFFFFF',
	},
	actionColor: {
		fontSize: 20,
		padding: 10,
		backgroundColor: '#555555',
		color: '#FFFFFF',
	},
});

export default ActionButton;
