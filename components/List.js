/**
 * Gingerasia Shopping list
 * Copy: Tatsumi Suzuki, Cecilia Fredriksson
 * Date: 2016-07-05
 */

import React, { Component } from 'react';

import {
	StyleSheet,
	Text,
	ListView,
	View
} from 'react-native';

import ListItem from "./ListItem";

class List extends React.Component {

	render() {

		return(
			<ListView
				dataSource={this.props.items}
				renderRow={(rowData, sectionID, rowID) => {
				return (
					<ListItem
						item={rowData}
						onPress={() => this.props.onPressItem(rowData, rowID)}
					/>
				)
			}}
			/>
		);
	}
}

export default List;
