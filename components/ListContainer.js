/**
 * Gingerasia Shopping list
 * Copy: Tatsumi Suzuki, Cecilia Fredriksson
 * Date: 2016-07-05
 */

import React, { Component } from 'react';

import {
	StyleSheet,
	Text,
	ListView,
	View
} from 'react-native';

const Realm = require('realm');

import List from "./List";
import ActionButton from "./ActionButton";


class ListContainer extends React.Component {

	render() {
		var realm = new Realm({
			schema: [{name: 'Dog', properties: {name: 'string'}}]
		});

		realm.write(() => {
			realm.create('Dog', {name: 'Lasse'});
		});

		this.itemsRef = realm.objects('Dog');

		this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
			 for(var i=0; i < this.itemsRef.length; i++) {
				console.log(this.itemsRef[i].name);
				this.state = {
					dataSource: this.dataSource.cloneWithRows({
					items: this.itemsRef[i].name
				})
			 };
		 }

		return(
			<View style={styles.container}>
				<Text style={styles.welcome}>
					{this.props.title}
				</Text>
				<Text style={styles.instructions}>
					Plats för Google Maps
					Count of Dogs in Realm: {realm.objects('Dog').length}
				</Text>
				<List
					items={this.state.dataSource}
					onPressItem={this.openItem}
				/>
				<ActionButton title="Skapa nytt" />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF',
	},
	welcome: {
		fontSize: 20,
		padding: 10,
		backgroundColor: '#ababab',
	},
});

export default ListContainer;
