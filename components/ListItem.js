/**
 * Gingerasia Shopping list
 * Copy: Tatsumi Suzuki, Cecilia Fredriksson
 * Date: 2016-07-05
 */

import React, { Component } from 'react';

import {
	StyleSheet,
	View,
	TouchableHighlight,
	Text
} from 'react-native';

class ListItem extends React.Component {

	render() {
		return(
			<TouchableHighlight onPress={ () => this.onPress() }>
				<Text style={styles.text}>{this.props.item}</Text>
			</TouchableHighlight>
		);
	}

	onPress() {
		console.log("Press item")
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF',
	},
	text: {
		padding: 30,
		borderStyle: 'solid',
		borderWidth: 1,
		borderBottomColor: '#dddddd'
	},
});

export default ListItem;
